import os
import subprocess as spr
import glob

class LAMMPS_runner:

    def __init__(self,
                 lammpsEXE_,
                 inputParams_,
                 templateFile_,
                 wkdir_=".",
                 createWkdir_=False):

        self.__requiredRarams = \
            {
                'dt',
                'boxLength',
                'nParticles',
                'randSeed',
                'rd',
                'A',
                'B',
                'gamma',
                'preRunSteps',
                'runSteps',
                'dumpFrequency',
                'thermoFrequency',
                'dumpFileName',
            }
        missingParams_ = self.__requiredRarams - set(inputParams_.keys())
        if len(missingParams_) > 0:
            raise ValueError('the following additional parameters must be set:\n' + \
                             str(missingParams_))

        self.inputParams = inputParams_

        self.templateFileText = open(templateFile_).read()
        self.lammpsEXE = lammpsEXE_
        self.wkdir = wkdir_
        self.createWkdir = createWkdir_

        assert (os.path.exists(self.lammpsEXE))
        if not createWkdir_:
            assert (os.path.exists(self.wkdir))

    def create_input_file(self, inputFilePath):
        # reverse sort the dictionary by key length. this enures that
        # if one key happens to be a substring of another key, that substring won't
        # erroneusly replace part of the superstring when the input file is created

        inFileText__ = self.templateFileText

        key = 'restartFrequency'
        if key in self.inputParams:
            # uncomment restart command
            inFileText__ = inFileText__.replace('#restart', 'restart')

            if 'restartPrefix' not in self.inputParams:
                restartPrefix_ = self.inputParams['dumpFileName']
                restartPrefix_ = restartPrefix_.replace('.xyz', '') + '.restart'
                self.inputParams['restartPrefix'] = restartPrefix_

                print('---------------------------------------------------------')
                print("Note: setting 'restartPrefix' to", restartPrefix_)
                print('---------------------------------------------------------')


        else:
            print('---------------------------------------------------------')
            print('Note: restart output is not enabled')
            print('---------------------------------------------------------')

        # if the input parameters specifies a restart, then change the input file accordingly
        key = 'restart'
        if key in self.inputParams.keys():

            self.inputParams[key] = str(self.inputParams[key])
            val = self.inputParams[key]
            # set file from which to restart
            restartFiles_ = glob.glob(val)
            if len(restartFiles_) < 1:
                raise FileNotFoundError("restart file, '" + self.inputParams['restart'] + \
                                        "' does not exist.\n" + \
                                        "Note: setting 'restart' with a '.*' wildcard will" + \
                                        "force a restart from the latest output")

            if '*' in val:
                print('restarting from one of these files:')
                for restartFile_ in restartFiles_:
                    print(restartFile_)

            # comment out certain commands. If this is not done, LAMMPS will refuse to run
            commandsToRemove_ = ['region', 'create_box', 'create_atoms']
            for cmd_ in commandsToRemove_:
                inFileText__ = inFileText__.replace(cmd_, '#' + cmd_)

            # uncomment read_restart command
            inFileText__ = inFileText__.replace('#read_restart', 'read_restart')

            val = self.inputParams['restart']

        scratch_ = dict()
        for k_ in sorted(self.inputParams, key=len, reverse=True):
            scratch_[k_] = self.inputParams[k_]

        self.inputParams = scratch_

        for key, val in self.inputParams.items():
            replaceStr = "__" + key
            inFileText__ = inFileText__.replace(replaceStr, str(val))

        inputFile = open(inputFilePath, 'w')
        inputFile.write(inFileText__)
        inputFile.close()

    def run_lammps(self, nProcs_, inputFilePath_):
        lammpsProcess__ = spr.Popen(['mpirun', '-np', str(nProcs_),
                                     self.lammpsEXE, '-i', inputFilePath_],
                                    stdout=spr.PIPE,
                                    stderr=spr.PIPE)
        #         lammpsProcess__ = spr.run(['mpirun','-np',str(nProcs),
        #                                      self.lammpsEXE,'-i',inputFilePath],
        #                                      capture_output=True, shell=True)
        return lammpsProcess__





