import numpy as np
import traceback

def uniform_2d_sample(npts, radius=1, theta0=0):
    '''
    Samples a number of points on the circumference of a circle centered at the origin.
    :param npts: number of points to sample
    :param radius: circle radius
    :param theta0: anglular position of first point
    :return: a tuple of the x and y coordinates (both of type np.ndarray) of the sampled points
    '''
    assert(npts>2)
    x = np.ndarray(npts)
    y = np.ndarray(npts)
    for i in range(npts):
        angle = theta0 + 2*np.pi*i/npts
        x[i] = np.cos(angle) * radius
        y[i] = np.sin(angle) * radius
    return x,y

class Bounds:
    def __init__(self, minVals, maxVals):
        self.min = np.array(minVals)
        self.max = np.array(maxVals)
        assert(np.all(self.min <= self.max))

class FunctionEvaluationError(Exception):
    '''
    a custom exception that is raised if evaluating an objective function results in an error
    '''
    pass

class Minimizer:
    def __init__(self, func, funcParams=[]):
        '''

        :param func: function to be minimized
        :param funcParams: extra parameters the objective function needs for evaluation. These parameters are not
               varied when the minimization is performed
        '''
        self.funcEvals = 0
        self.paramVals = []
        self.funcVals = []
        self.gradientVals = []
        self.direction = []
        self.nEvals = 0
        self.currentparamVals = []
        self.currentFuncVals = []
        self.guesses = []
        self.nArgs = None
        self.nIter = 0

        def the_func(args):
            try:
                return func(args, *funcParams)
            except Exception as e:
                # if the error is an instance of KeyboardInterupt, raise that error
                if isinstance(e, KeyboardInterrupt):
                    raise e
                else:
                    print('caught the following error while evaluating the objective function:\n')
                    traceback.print_exc()
                    raise FunctionEvaluationError

        def funcWithCount(args):
            val = the_func(args)
            self.currentparamVals.append(args)
            self.currentFuncVals.append(val)
            self.nEvals += 1
            return val

        self.funcWithCount = funcWithCount
        self.func = the_func

    def evaluate_func_on_stencil(self, guess, stencilIndexes, dxMeshgrid, verbose):
        '''

        :param guess: a guess for the objective function's minimum
        :param stencilIndexes: indexes of the stencil being used. for a 1st order forward difference problem in 2D,
               stencil indexes correspond to the 'o's in the diagram below:
                                      +----+----+
                                      |    |    |
                                      o----+----+
                                      |    |    |
                                      o----o----+
        :param dxMeshgrid: a meshgrid of delta x values
        :param verbose: flag for printing additional information
        :return: function values evaluated on the implemented stencil
        '''
        funcVals = np.ndarray([self.nArgs for _ in range(self.nArgs)])
        funcVals.fill(np.nan)
        for idx_i in stencilIndexes:
            idx = tuple(idx_i)
            args = np.array([guess[j] + dxMeshgrid[j][idx] for j in range(self.nArgs)])
            if np.max(np.abs(idx)) == 0 and self.nIter > 0:
                # if evaluating at the parameter guess passed to this function and this is not the
                # first iteration, skip the function evaluation
                if verbose:
                    print("Settting function value to best value from previous iteration")

                latestParamVals = self.paramVals[-1]
                latestFuncVals = self.funcVals[-1]
                i_list = [i for i, p_i in enumerate(latestParamVals) if (p_i == guess).all()]
                assert (len(i_list) > 0)
                i_guess = i_list[-1]
                funcVals[idx] = latestFuncVals[i_guess]
                self.currentparamVals.append(guess)
                self.currentFuncVals.append(funcVals[idx])
            else:
                funcVals[idx] = self.funcWithCount(args)

        return funcVals


    def compute_bounded_delta(self, oldGuess, delta, bounds):
        '''

        :param oldGuess: old parameter guesses for the function minimum
        :param delta: computed parameter delta -- oldGuess +  delta is the new guess
        :param bounds: prescibed parameter bounds
        :return: a delta that yields a new guess within the prescribed bounds and in the same direction as the delta
                 value passed to this function
        '''
        step = np.linalg.norm(delta)
        stepMultiplier = 1
        for j in range(self.nArgs):
            indepVar = oldGuess[j] + delta[j]
            if indepVar > bounds.max[j]:
                print('new guess for variable', j, 'is greater than specified maximum: ', indepVar, '>', bounds.max[j])
                stepMultiplier = min(stepMultiplier, abs((oldGuess[j] - bounds.max[j]) / delta[j]))
            elif indepVar < bounds.min[j]:
                print('new guess for variable', j, 'is less than specified maximum: ', indepVar, '<', bounds.min[j])
                stepMultiplier = min(stepMultiplier, abs((oldGuess[j] - bounds.min[j]) / delta[j]))

        if stepMultiplier < 1:
            print('Guess exceeds variable bounds. multiplying step size by', stepMultiplier)
            step *= stepMultiplier
            print('which yields a step size of', step)

        return delta*stepMultiplier


    def minimize_iteration_conjugate_gradient(self,
                                              guess,
                                              dx,
                                              stepRange,
                                              sigma,
                                              bounds: Bounds,
                                              betaForm='FR',
                                              verbose=False):
        '''
        computes a new guess for a function minimum using the conjugate gradient method:

        x_k = x_{k-1} + d_k,
        d_k = g_k + \beta*d_{k-1},
        where
        d_0 = -g_0,
        d_k denotes the step taken to obtain a new guess, x_k,
        g_k is the gradient of the objective function evaluated at x_k,
        and \beta is a scalar which depends on the current and previous gradients, g_k and g_{k-1}, respectively,
        and the previous direction, d_{k-1}. The exact form  of beta depends on the form specified.

        if x_k > x_{k-1}, x_k is iteratively recomputed by
        - setting d_k = d_k/{\sigma} and then
        - computing x_k = x_{k-1} + d_k
        until x_k < x_{k+1}

        :param guess: guess for the function minimum
        :param dx: values of delta x for each variable
        :param stepRange: an array with the min and max of the step sizes taken for the solution algorithm
        :param sigma: factor by which the step size is reduced when performing a line search -- step size (sz) is set
               sz /= sigma until sz < min(stepRange) or a decrease in the function value is detected
        :param bounds: bpunds given for each parameter
        :param betaForm: the form of beta (old gradient ) used. Currently the options are
               - 'FR' (Fletcher-Reeves) and,
               - 'WYL' (Wei-Yao-Liu).
              The 'FR' and 'WYL' forms of \beta are taken form equations (4)  and (10), respectively, from the
              following publication: https://www.hindawi.com/journals/aaa/2014/279891/
        :param verbose: flag for printing additional information
        :return: a new set of parameters
        '''

        availableBetaForms = {'FR':"Fletcher-Reeves", 'WYL':"Wei-Yao-Liu"}
        if betaForm not in availableBetaForms.keys():
            msg = "'betaForm' must be one of the following:\n"
            msg += str(list(key for key in availableBetaForms.keys()))
            raise ValueError(msg)

        nArgs = len(guess)
        self.nArgs = nArgs

        minStep = np.min(stepRange)
        maxStep = np.max(stepRange)
        if np.isinf(maxStep):
            print('no limit set on max step size.')

        self.guesses.append(guess)

        #         H = [np.linspace(guess[i]-dx,guess[i]+dx,3) for i in range(self.nArgs)]
        DX = [np.linspace(0, dx[i], 2) for i in range(nArgs)]
        DX = np.meshgrid(*DX, indexing='ij')

        funcVals = np.ndarray([nArgs for _ in range(nArgs)])
        funcVals.fill(np.nan)

        # for a 2D stencil,the function is evaluated on the grid points
        # where 'o' appears
        #      +----+----+
        #      |    |    |
        #      o----+----+
        #      |    |    |
        #      o----o----+

        I = np.eye(nArgs, dtype=np.int)
        idxArray = np.zeros([1, nArgs], dtype=np.int)
        idxArray = np.vstack([idxArray, I])

        funcVals = self.evaluate_func_on_stencil(guess, idxArray, DX, verbose)

        grad = np.ndarray([self.nArgs])
        idx_zero = tuple(0 for _ in range(self.nArgs))


        for j in range(nArgs):
            j_idx = tuple(_ for _ in I[j])
            grad[j] = (funcVals[j_idx] - funcVals[idx_zero]) / dx[j]

        # if a direction has not been computed, set the step size
        direction = -grad
        if len(self.direction) > 0:
            if verbose:
                print('computing conjugate gradient contribution')

            grad_nm1 = self.gradientVals[-1]
            if betaForm == 'FR':
                beta =np.linalg.norm(grad)**2 / np.linalg.norm(grad_nm1)**2 # Fletcher-Reeves beta
            elif betaForm == 'WYL':
                yk = grad - np.linalg.norm(grad)/ np.linalg.norm(grad_nm1)*grad_nm1
                beta = np.matmul(grad.T, yk) / np.linalg.norm(grad_nm1)**2 # Wei-Huang beta
            else:
                raise RuntimeError('beta has not been defined!!!')

            direction_nm1 = self.direction[-1]
            direction += beta * direction_nm1

        i_min = np.argmin(self.currentFuncVals)
        minFuncVal = self.currentFuncVals[i_min]
        bestParams = self.currentparamVals[i_min]
        bestDelta = bestParams - guess

        deltaList = [bestDelta]
        funcValList = [minFuncVal]

        unitDirection = direction / np.linalg.norm(direction)

        if np.isinf(maxStep):
            step = np.linalg.norm(direction)
        else:
            step = maxStep

        delta = step * unitDirection

        # check if the computed step is within the prescribed bounds
        delta = self.compute_bounded_delta(guess, delta, bounds)
        step = np.linalg.norm(delta)

        while True:

            if step < minStep:
                if step > minStep / sigma:
                    print('setting step to minimum value.')
                    step = minStep
                else:
                    break

            delta = step * unitDirection
            deltaList.append(delta)
            args = np.array([guess[j] + delta[j] for j in range(nArgs)])
            try:
                fv = self.funcWithCount(args)
            except FunctionEvaluationError as err:
                # set `fv` to infinity -- this forces the reduction of `step` to something that hopefully won't
                # result in an error
                fv = np.inf

            funcValList.append(fv)
            if verbose:
                print('delta_i:', delta)

            if fv >= minFuncVal:
                if verbose:
                    print('newest function evaluation >= last evaluation')
                step /= sigma
            else:
                if verbose:
                    print('newest function evaluation < last evaluation')
                bestDelta = delta
                break

        self.gradientVals.append(grad)
        self.direction.append(direction)
        self.paramVals.append(self.currentparamVals)
        self.funcVals.append(self.currentFuncVals)

        self.currentparamVals = []
        self.currentFuncVals = []

        if verbose:
            print('best delta:', bestDelta)
        return guess + bestDelta

    def minimize_iteration_newtons_method(self,
                                          guess,
                                          dxVals,
                                          bounds: Bounds,
                                          verbose=False):
        '''
        computes a new guess for a minimum using Newton's method.

        :param guess: guess for the function minimum
        :param dx: values of delta x for each variable
        :param bounds: bpunds given for each parameter
        :param bounds: bpunds given for each parameter
        :return: a new set of parameters
        '''
        # this assumes that dx in each direction is identical
        assert (np.min(dxVals) == np.max(dxVals))
        dx = np.min(dxVals)

        nArgs = len(guess)
        self.nArgs = nArgs

        self.guesses.append(guess)

        DX = [np.linspace(0, 2 * dx, 3) for i in range(self.nArgs)]
        DX = np.meshgrid(*DX, indexing='ij')

        # for a 2D stencil,the function is evaluated on the grid points
        # where 'o' appears
        #      o----+----+
        #      |    |    |
        #      o----o----+
        #      |    |    |
        #      o----o----o

        I = np.eye(nArgs, dtype=np.int)

        # the next few lines fill an array of indexes, idxArray,  such that sum(idx) <= 2 for all idx in idxArray
        idxArray = np.zeros([1, nArgs], dtype=np.int)
        idxArray = np.vstack([idxArray, I])
        idxArray = np.vstack([idxArray, 2 * I])
        for i in range(nArgs):
            for j in range(i + 1, nArgs):
                idxArray = np.vstack([idxArray, I[j] + I[i]])

        funcVals = np.ndarray(DX[0].shape)
        funcVals.fill(np.nan)

        for idx_i in idxArray:
            idx = tuple(idx_i)
            args = np.array([guess[j] + DX[j][idx] for j in range(nArgs)])
            funcVals[idx] = self.funcWithCount(args)
        # funcVals = self.evaluate_func_on_stencil(guess, idxArray, DX, verbose)

        hess = np.ndarray([self.nArgs, self.nArgs])
        grad = np.ndarray([self.nArgs])

        i_zero = tuple(0 for _ in range(self.nArgs))
        for i in range(nArgs):
            i_dx = tuple(I[i])
            i_2dx = tuple(2 * I[i])
            # compute diagonal components of Hessian matrix
            hess[i, i] = (funcVals[i_2dx] - 2 * funcVals[i_dx] + funcVals[i_zero])/dx ** 2

            # compute gradient
            grad[i] = (-funcVals[i_2dx] + 4 * funcVals[i_dx] - 3 * funcVals[i_zero]) / (2 * dx)

            for j in range(i + 1, nArgs):
                j_dx = tuple(I[j])
                ij_dx = tuple(I[i] + I[j])
                # compute off-diagonal components of Hessian matrix
                hess[i, j] = (funcVals[i_zero] + funcVals[ij_dx] - funcVals[i_dx] - funcVals[j_dx]) / dx ** 2
                hess[j, i] = hess[i, j] # Hessian matrix is symmetric

        if verbose:
            print('hessian:')
            print(hess)

        self.gradientVals.append(grad)
        self.paramVals.append(self.currentparamVals)
        self.funcVals.append(self.currentFuncVals)

        self.currentparamVals = []
        self.currentFuncVals = []

        hessInv = np.linalg.inv(hess)
        delta = -np.matmul(hessInv, grad)

        delta = self.compute_bounded_delta(guess, delta, bounds)

        return guess + delta


    def minimize_iteration_bracketing(self, guess, funcVal, dxMax, nSample, angle):

        self.currentparamVals = []
        self.currentFuncVals = []

        dx = uniform_2d_sample(nSample, radius=dxMax, theta0=angle)
        self.guesses.append(guess)

        self.currentparamVals.append(guess)
        self.currentFuncVals.append(funcVal)

        newParamVals = guess[:, None] + np.hstack([dx])

        for params in newParamVals.T:
            val = self.funcWithCount(params)

        self.paramVals.append(self.currentparamVals)
        self.funcVals.append(self.currentFuncVals)

        j = np.argmin(self.currentFuncVals)
        oldGuessIsMin = funcVal == self.currentFuncVals[j]

        if oldGuessIsMin:
            print('initial guess is still minimum')
        else:
            print('initial guess is no longer minimum')

        return self.currentparamVals[j], self.currentFuncVals[j], oldGuessIsMin

    def minimize(self, guess, dxMax, gradTol=1e-3, dxTol=1e-3, maxNewtonIter=5, scheme='newton', kwargs={}):
        '''

        :param guess: intial guess for the function minimum
        :param dxMax:
        :param gradTol:
        :param dxTol:
        :param maxNewtonIter:
        :param scheme:
        :param otherArgs:
        :return:
        '''
        assert (guess.ndim == 1)
        self.nIter = 0

        timesParamTolAchieved = 0

        self.isFirstIter = True  # conjugate gradient relies on this

        if scheme == 'newton':
            minimizer = self.minimize_iteration_newtons_method

        elif scheme == 'cg':
            minimizer = self.minimize_iteration_conjugate_gradient
        else:
            raise RuntimeError('unrecognized solver scheme')

        while True:
            if self.nIter >= maxNewtonIter:
                print('max number of newton iterations exceeded')
                break

            print('iteration:', self.nIter + 1)
            newGuess = minimizer(guess, dxMax, **kwargs)
            dGuess = np.linalg.norm(newGuess - guess, ord=np.inf)
            guess = newGuess
            self.nIter += 1
            if dGuess <= dxTol:
                timesParamTolAchieved += 1
                print('parameter change tolerance achieved (', dGuess, '<=', dxTol, ')')
                if scheme == 'cg' and timesParamTolAchieved < 2:
                    continue
                else:
                    break

            gradNorm = np.abs(self.gradientVals[-1]).max()
            print('gradient :', self.gradientVals[-1])
            print('new guess:', newGuess)
            if gradNorm <= gradTol:
                print('gradient tolerance achieved (', gradNorm, '<=', gradTol, ')')
                break

        return newGuess
