import numpy as np
from numba import cuda
import math


@cuda.jit(device=False)
def interparticle_distance(r_ij_, x_, y_, z_, i_, dims):
    timeIdx_, j_ = cuda.grid(2)

    if timeIdx_ < x_.shape[0] and j_ < x_.shape[1]:
        distX = abs(x_[timeIdx_, i_] - x_[timeIdx_, j_])
        distY = abs(y_[timeIdx_, i_] - y_[timeIdx_, j_])
        distZ = abs(z_[timeIdx_, i_] - z_[timeIdx_, j_])

        distX = min(distX, dims[0] - distX)
        distY = min(distY, dims[1] - distY)
        distZ = min(distZ, dims[2] - distZ)

        r_ij_[timeIdx_, j_] = math.sqrt(distX ** 2 + distY ** 2 + distZ ** 2)

# ---------------------------------------------------------

@cuda.jit(device=False)
def gpu_product(result_, a_, b_):
    timeIdx_, j_ = cuda.grid(2)

    if timeIdx_ < a_.shape[0] and j_ < a_.shape[1]:
        result_[timeIdx_, j_] = a_[timeIdx_, j_] * b_[timeIdx_, j_]

# ---------------------------------------------------------


@cuda.jit(device=False)
def compute_lennard_jones_force(force_ij_, r_ij_, sigma_, eps_, i_):
    timeIdx_, j_ = cuda.grid(2)

    if timeIdx_ < r_ij_.shape[0] and j_ < r_ij_.shape[1]:
        sigma_ij = 0.5 * (sigma_[i_] + sigma_[j_])
        eps_ij_ = math.sqrt(eps_[i_] * eps_[j_])

        srInv6 = (sigma_ij / r_ij_[timeIdx_, j_]) ** 6
        force_ij_[timeIdx_, j_] = 4 * eps_ij_ * (12 * srInv6 ** 2 - 6 * srInv6) / r_ij_[timeIdx_, j_]

# ---------------------------------------------------------

@cuda.jit(device=False)
def fix_intermolecular_force(force_ij_, moleculeID_, i_):
    timeIdx_, j_ = cuda.grid(2)

    if timeIdx_ < force_ij_.shape[0] and j_ < force_ij_.shape[1]:
        # if particles i_ and j_ belong to the same molecule, set the force to zero
        # this is done because the potential used (Trappe-UA) does not consider an
        # LJ force between atoms of the same molecule
        if moleculeID_[i_] == moleculeID_[j_]:
            #             force_ij_[timeIdx_, j_] = 0
            force_ij_[timeIdx_, j_] = 0

# ---------------------------------------------------------

class ParticleMixProperties:
    def __init__(self,
                 xPos,
                 yPos,
                 zPos,
                 pType,
                 moleculeID,
                 domainDims,
                 sigmaDict,
                 epsilonDict,
                 dtype=np.float32,
                 isPeriodic=True):
        '''

        :param xPos x-positions of atoms/particles
        :param yPos:
        :param zPos:
        :param pType:
        :param moleculeID:
        '''
        intType = np.int16

        # ensure keys in properties match particle type values
        pTypeUnique = np.unique(pType)
        pTypeUnique.sort()
        tmp = np.array( list(epsilonDict.keys()) )
        tmp.sort()

        assert( np.min(tmp == pTypeUnique) == True )
        tmp = np.array( list(sigmaDict.keys()) )
        tmp.sort()
        assert( np.min(tmp == pTypeUnique) == True )

        # lots of changes will be necessary if the number of particles changes with each time step...
        assert(xPos.ndim == 2)
        assert(yPos.ndim == 2)
        assert(zPos.ndim == 2)
        assert(pType.ndim == 1)
        assert (moleculeID.ndim == 1)

        typeToPropMap = np.arange(pTypeUnique.size, dtype=intType)

        self.sigma        = -np.ones(pType.shape, dtype=dtype)
        self.epsilon      = -np.ones(pType.shape, dtype=dtype)
        self.particleType = -np.ones(pType.shape, dtype=intType)

        # this takes the curent particle types and makes them a sequence of ints starting from 0
        for key in typeToPropMap:
            pt = pTypeUnique[key]
            choice = pType == pt
            self.sigma       [choice] = sigmaDict  [pt]
            self.epsilon     [choice] = epsilonDict[pt]
            self.particleType[choice] = key

        self.x = np.array(xPos, dtype=dtype)
        self.y = np.array(yPos, dtype=dtype)
        self.z = np.array(zPos, dtype=dtype)

        # self.particleType = np.array(pType     , dtype=intType)
        self.moleculeID   = np.array(moleculeID, dtype=intType)

        self.dims = np.array(domainDims, dtype=dtype )

        self.isPeriodic = isPeriodic

        self.x_gpu = None
        self.y_gpu = None
        self.z_gpu = None

        self.particleType_gpu = None
        self.moleculeID_gpu   = None

        self.dims_gpu = None

        self.sigma_gpu   = None
        self.epsilon_gpu = None

        self.gpu_initalized = False

    def initialize_gpu_arrays(self, deviceID):
        cuda.select_device(deviceID)
        self.x_gpu = cuda.to_device(self.x)
        self.y_gpu = cuda.to_device(self.y)
        self.z_gpu = cuda.to_device(self.z)

        self.particleType_gpu = cuda.to_device(self.particleType)
        self.moleculeID_gpu   = cuda.to_device(self.moleculeID  )

        self.sigma_gpu   = cuda.to_device(self.sigma  )
        self.epsilon_gpu = cuda.to_device(self.epsilon)

        self.dims_gpu = cuda.to_device(self.dims)

        self.gpu_initalized = True

