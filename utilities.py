import os
import numpy as np
import matplotlib as mpl
from scipy import constants

from scipy.optimize import curve_fit

import sympy as sym
from sympy.utilities.autowrap import ufuncify
import time

mpl.rc('text', usetex=True)

font = {'size': 12}

mpl.rc('font', **font)
mpl.rc('text', usetex=True)


rSym = sym.symbols('r')

# --------------------------------------------------------------------------------------------

def parse_lammps_output(path, atomsPerMolecule, debug=False):
    '''
    LAMMPS output is structured as follows:

    ITEM: TIMESTEP
    {time step number}
    ITEM: NUMBER OF ATOMS
    {number of atoms}
    ITEM: BOX BOUNDS {x boundary type} {y boundary type} {z boundary type}
    {x lower bound} {x upper bound}
    ITEM: ATOMS {list of ouputted variables}
    {output for atom 1}
    {output for atom 2}
    ...
    {output for atom n}
    n
    number of atoms (n)
    Timestep: (time i+1)
    (atom 1 type) x y z
    (atom 2 type) x y z
    ...
    (atom n type) x y z

    This function creates a dict of numpy arrays for the output variables as well as the time
    The structure of each array in the dict is as follows:

    varArray: [time step][molecule][atom]

    '''
    d = open(os.path.expanduser(path), 'r').read()
    d = d.split("ITEM: TIMESTEP\n")

    # first line is empty
    del d[0]

    i_datBegin = 8

    d_i = d[0].split("\n")
    nAtoms = int(d_i[2])
    nMol = int(nAtoms / atomsPerMolecule)
    nTimes = len(d)
    times = np.ndarray(nTimes)

    varNames = d_i[7].split(' ')[2:]
    del varNames[-1]

    dataList = []
    myData = np.ndarray((nAtoms, len(varNames)))

    for i in range(nTimes):
        d_i = d[i].split("\n")
        times[i] = int(d_i[0])

        if (i+1)%100 == 0:
            print("getting data for time = ", i + 1, '/', nTimes)

        if debug:
            # the for-loop below prints info for each timestep
            for line in d_i[0:i_datBegin]:
                print(line)

            # some sanity-checking
            if nAtoms != int(d_i[2]):
                raise ValueError('number of atoms not equal across timesteps: ',
                                 "\nexpected:", nAtoms,
                                 "\ngot:", d_i[2])
            varNames_i = d_i[7].split(' ')[2:]
            del varNames_i[-1]
            if varNames_i != varNames:
                raise ValueError('outputted variables not consistent across timesteps: ',
                                 "\nexpected:", varNames,
                                 "\ngot:", varNames_i)

        # print(d_i[-1])
        # the structure of this line will be 'ITEM: ATOMS [list of variables] {empty space}'
        for i_arr, line in enumerate(d_i[i_datBegin:nAtoms + i_datBegin]):
            myData[i_arr] = np.fromstring(line, sep=' ')

        dataList.append(myData.reshape((nMol, atomsPerMolecule, len(varNames))).copy())

    dataList = np.array(dataList)
    dataDict = {'t': times}
    for i, varName in enumerate(varNames):
        dataDict[varName] = dataList[..., i]

    return dataDict

# --------------------------------------------------------------------------------------------

def parse_lammps_log(path_, ignoreFirstOutput_=False):
    '''
    parses the thermo output of lammps
    :param path_ -- path to lammps log (str) or bytes output with the lammps log
    :param ignoreFirstOutput_ -- if True, ignores the first set of LAMMPS thermo output. This is useful
                             when the first run is
    '''
    if type(path_) is str:
        log = open(os.path.expanduser(path_), 'r').read()
    elif type(path_) is bytes:
        log = path_.decode('ascii')
    else:
        raise TypeError('Invalid type for variable "path_". Valid types are {str, bytes}')

    log = log.split('\n')

    beginList = []
    endList = []
    for i, line in enumerate(log):
        if "Dangerous" in line: print(line)
        if "Per MPI rank memory" in line: beginList.append(i + 1)
        if "Loop time" in line: endList.append(i)

    if len(beginList) == 0 or len(endList) == 0:
        raise ValueError('No thermo data to be parsed. Are you sure this a LAMMPS output?')

    # delete the first
    if ignoreFirstOutput_:
        del beginList[0]
        del endList[0]

    assert (len(beginList) == len(endList))
    if len(beginList) == 0:
        raise ValueError('No thermo data to be parsed. Try running this function again with \
                         "ignoreFirstOutput" set to "False"')

    varNames = log[beginList[0]].split(' ')
    del varNames[-1]

    nOut = 0
    j_ = 0
    for bl, el in zip(beginList, endList):
        nOut += el - bl - 1

    arr_ = np.ndarray((nOut, len(varNames)))
    for i_ in range(len(beginList)):
        for line in log[beginList[i_] + 1:endList[i_]]:
            arr_[j_] = np.fromstring(line, sep=' ')
            j_ += 1

    outDict_ = dict()
    for i_, varName in enumerate(varNames):
        outDict_[varName] = arr_[..., i_]
    return outDict_

# --------------------------------------------------------------------------------------------

def compute_r_ij_component(coords, index_i, domainDims_, dir):
    '''
    computes distance components between particle corresponding to index_i and the set of particles considered within a
    periodic cubic domain.

    coords    : array of particle coordinates with dimensions [n_particles, 3]
    index_i   : index of particle of interest
    domainDim_: edge length(s) of periodic domain. This can be a single float or an iterable
    '''
    if type(domainDims_) in (int, float):
        dd = domainDims_
    else:
        dd = domainDims_[dir]

    dist = coords[index_i, ..., dir] - coords[:, ..., dir]
    sgn = np.sign(dist)
    absDist = np.abs(dist)
    distComplement = dd - absDist
    isCloser = distComplement < absDist
    dist[isCloser] = -sgn[isCloser]*distComplement[isCloser]
    return dist


# --------------------------------------------------------------------------------------------

def compute_r_ij_components(coords, index_i, domainDims_):

    '''
    computes distance components between particle corresponding to index_i and the set of particles considered within a
    periodic cubic domain.

    coords    : array of particle coordinates with dimensions [n_particles, 3]
    index_i   : index of particle of interest
    domainDims_: edge length(s) of periodic domain. This can be a single float or an iterable
    '''

    distX = compute_r_ij_component(coords, index_i, domainDims_, 0)
    distY = compute_r_ij_component(coords, index_i, domainDims_, 1)
    distZ = compute_r_ij_component(coords, index_i, domainDims_, 2)

    return distX, distY, distZ
# """

# --------------------------------------------------------------------------------------------

def compute_r_ij(coords, index_i, domainDims_):
    '''
    computes distace between particle corresponding to index_i and the set of particles considered within a
    periodic cubic domain.

    coords    : array of particle coordinates with dimensions [n_particles, 3]
    index_i   : index of particle of interest
    domainDim_: edge length(s) of periodic domain. This can be a single float or an iterable
    '''
    distX, distY, distZ = compute_r_ij_components(coords, index_i, domainDims_)

    return np.sqrt(np.square(distX) + np.square(distY) + np.square(distZ))


# --------------------------------------------------------------------------------------------

# compute local density needed for DPD force calculations
def local_density_kernel(dist, cutoffDist):
    '''
    computes the local density based on the distance between some particle i and all particles (dist)
    as some cutoff distance (cutoffDist) based on the following formula:

    local_density_i = \sum_{j != i} w_p(r_ij)

    where

    w_p(r) = 15/(2pi*cutOffDist**3)*(1-r/cutOffDist)**2, if dist < cutOffDist,
           = 0, otherwise
    '''
    i_nonzero = (dist < cutoffDist)

    # subtract 1 from the sum to remove particle index i=j contribution
    tmp = np.sum(np.square(1 - dist[i_nonzero] / cutoffDist)) - 1

    return 15 / (2 * np.pi * cutoffDist ** 3) * tmp


# --------------------------------------------------------------------------------------------

def lennard_jones_potential_expr():
    '''
    :return:
    a symbolic expression for a Lennard-Jones potential [with units Joules]
    variables --
    r       : distance between particles
    sigma   : distance at which potential is zero
    epsilon : potential well depth
    '''
    r, eps, sig = sym.symbols(['r', 'epsilon', 'sigma'])
    return 4 * eps * ((sig / r) ** 12 - (sig / r) ** 6)


# --------------------------------------------------------------------------------------------

def coulomb_potential_expr():
    '''
    :return:
    a symbolic expression for a Coulumb potential [with units Joules]
    r   : distance between particles
    q_1 : charge of particle 1 [units = Coulumbs]
    q_2 : charge of particle 2 [units = Coulumbs]
    '''
    ke = 8.988e-9
    r, q_2, q_1 = sym.symbols(['r', 'q_2', 'q_1'])
    return ke * q_1 * q_2 / r

# --------------------------------------------------------------------------------------------

def molecular_center_of_mass(coords_, atomMass_, periodicDims_=None, maxMoleculeLen_=0):
    '''
    Computes the centers of mass of molecules 
    :param coords_: 
    :param atomMass_: 
    :param periodicDims_: 
    :param maxMoleculeLen_: 
    :return: 
    '''
    nDims_ = coords_.shape[-1]
    assert(nDims_<4)
    nMol_ = atomMass_.shape[0]

    molMass_ = np.sum(atomMass_, axis=1)

    centerOfMass_ = np.ndarray([nMol_, nDims_])
    for i_dim  in range(nDims_):
        centerOfMass_[:, i_dim] = np.sum(coords_[..., i_dim] * atomMass_, axis=1) / molMass_

    if periodicDims_ is not None and maxMoleculeLen_>0.:
        if type(periodicDims_) in (float, int):
            pd_ = list([periodicDims_ for _ in range(nDims_)])
        else:
            pd_ = periodicDims_
            assert(len(pd_) == nDims_)

        moleculeSpanSqr = 0
        for k_ in range(nDims_):
            moleculeSpanSqr += (np.max(coords_[..., k_], axis=-1) - np.min(coords_[..., k_], axis=-1)) ** 2

        i_ = (moleculeSpanSqr > maxMoleculeLen_**2)
        c_ = coords_[i_]

        nNearBc = c_.shape[0]

        # translate particles more than halfway across the domain  by -pd_k
        for k_, pd_k in enumerate(pd_):
            j_ = c_[..., k_] > pd_k/2
            # c_[j_, k_] = pd_k - c_[j_, k_]
            c_[j_, k_] = c_[j_, k_] - pd_k

        centerOfMassNearBC_ = np.ndarray([nNearBc, nDims_])
        print(100*centerOfMassNearBC_.shape[0]/nMol_,'% cross a boundary')

        for i_dim in range(nDims_):
            centerOfMassNearBC_[:, i_dim] = np.sum(c_[..., i_dim] * atomMass_[i_], axis=1) / molMass_[i_]
            # translate centers of mass that are negative to the left by pd_k
            j_ = centerOfMassNearBC_[:, i_dim] < 0
            centerOfMassNearBC_[j_, i_dim] = pd_[i_dim] + centerOfMassNearBC_[j_, i_dim]

        centerOfMass_[i_, :] =centerOfMassNearBC_

    return centerOfMass_

# --------------------------------------------------------------------------------------------

def hat_expr_list(nodes_, includesGhosts_=False):
    '''
    :param rSym_          : symbolc variable representing interparticle distance
    :param nodes_         : a list of finite element nodes
    :param includesGhosts_: bool specifying whether or not 'nodes_" contains ghost nodes
    :return: a list of symbolic 'hat' (inverted absolute value) expressions centered at each node
    '''
    nodeArray_ = np.array(nodes_)
    if not includesGhosts_:
        # if ghost nodes are not included, add them
        nodeArray_ = np.insert(nodeArray_, 0, 2 * nodeArray_[0] - nodeArray_[1])
        nodeArray_ = np.insert(nodeArray_, nodeArray_.size, 2 * nodeArray_[-1] - nodeArray_[-2])

    hatList_ = []
    n_ = len(nodeArray_)
    for i in range(1, n_ - 1):
        c_ = nodeArray_[i]
        lb_ = nodeArray_[i - 1]
        ub_ = nodeArray_[i + 1]
        w_ = 2 / (ub_ - lb_)
        absFunc_ = 1 - abs(w_ * (rSym - c_))
        hatList_.append(sym.Piecewise((absFunc_, absFunc_ >= 0), (0, True)))
    return hatList_

# --------------------------------------------------------------------------------------------

__ljp = lennard_jones_potential_expr()
__ljf = -sym.diff(__ljp, 'r')

__symNames = ['sigma', 'epsilon', 'r']

__symList = []
for __nam in __symNames:
    for __s in list(__ljp.free_symbols):
        if __s.name is __nam:
            __symList.append(__s)

assert([__s.name for __s in __symList] == ['sigma', 'epsilon', 'r'])

lj_potential_func = ufuncify(__symList,__ljp,backend='numpy')
lj_force_func     = ufuncify(__symList,__ljf,backend='numpy')

# --------------------------------------------------------------------------------------------


def compute_mdpd_pressure_contributions(particleType_,
                                        coords_,
                                        pMassDict_,
                                        rc_,
                                        rd_,
                                        domainDims_,
                                        isPeriodic=True,
                                        approxMoleculeLen=0.):
    '''
    returns a tuple of attractive and repulsive contributions to the pressure
    according to the mDPD force kernel
    '''

    nMol_ = particleType_.shape[0]
    nDims_ = len(domainDims_)

    particleMass_ = np.ndarray(particleType_.shape) * np.nan
    domainDimsReduced_ = [dd_/rc_ for dd_ in domainDims_]

    for key in pMassDict_.keys():
        i_chosen = (particleType_ == key)

        particleMass_[i_chosen] = pMassDict_[key]

    if isPeriodic:
        comReduced_ = molecular_center_of_mass(coords_ / rc_,
                                               particleMass_,
                                               domainDimsReduced_,
                                               approxMoleculeLen/rc_)

    else:
        comReduced_ = molecular_center_of_mass(coords_ / rc_,
                                               particleMass_)

    volReduced_ = np.prod(domainDimsReduced_)

    rcReduced_ = 1
    rdReduced_ = rd_ / rc_

    avgFarNeighbors_ = 0
    avgCloseNeigbors_ = 0

    locDensDPD_ = np.ndarray([nMol_]) * np.nan

    for iMol in range(nMol_):
        r_ij = compute_r_ij(comReduced_, iMol, domainDimsReduced_)
        locDensDPD_[iMol] = local_density_kernel(r_ij, rdReduced_)

    # contribution to excess pressure from attractive part of force
    pExcessAttractive_ = np.ndarray([nMol_]) * np.nan
    # contribution to excess pressure from repuslive part of force
    pExcessRepulsive_ = np.ndarray([nMol_]) * np.nan

    for iMol in range(nMol_):
        #         print('molecule: ', iMol+1, '/', nMol_)
        r_ij = compute_r_ij(comReduced_, iMol, domainDimsReduced_)
        #     print("\tr_min:", rr.min())
        #     print("\tr_max:", rr.max())

        isNeighbor = r_ij < rcReduced_
        isNeighbor[iMol] = False # self isn't its own neighbor
        tmp = np.sum(isNeighbor)
        avgFarNeighbors_ += tmp
        #         print('\tfar neighbors:  ', tmp)
        pExcessAttractive_[iMol] = np.sum(r_ij[isNeighbor] / rcReduced_ * (1 - r_ij[isNeighbor] / rcReduced_))

        isNeighbor = r_ij < rdReduced_
        isNeighbor[iMol] = False # self isn't its own neighbor
        tmp = np.sum(isNeighbor)
        avgCloseNeigbors_ += tmp
        #         print('\tclose neighbors:', tmp)
        pExcessRepulsive_[iMol] = np.sum(r_ij[isNeighbor] / rcReduced_ \
                                         * (1 - r_ij[isNeighbor] / rdReduced_) \
                                         * (locDensDPD_[iMol] + locDensDPD_[isNeighbor]))

    avgFarNeighbors_ /= nMol_
    avgCloseNeigbors_ /= nMol_
    print('avg. close neighbors: ', avgCloseNeigbors_)
    print('avg. far   neighbors: ', avgFarNeighbors_)

    # pressure contributions multiplied by number of molecules, nMol_.
    # this is done to ensure
    # [mean excess pressure] = mean[ per-molecule pressure contributions ]
    # rather than
    # [mean excess pressure] =  sum[ per-molecule pressure contributions ]

    pExcessAttractive_ *= nMol_ / (2 * nDims_* volReduced_)
    pExcessRepulsive_ *= nMol_ / (2 * nDims_ * volReduced_)

    return pExcessAttractive_, pExcessRepulsive_


# --------------------------------------------------------------------------------------------


def mDPD_force(a_, b_, locDens_, rc_, rd_, r_):
    attractiveForce_ = a_ * (1 - r_ / rc_)
    attractiveForce_[r_ > rc_] = 0

    repulsiveForce_ = b_ * 2 * locDens_ * (1 - r_ / rd_)
    repulsiveForce_[r_ > rd_] = 0

    return attractiveForce_ + repulsiveForce_

# --------------------------------------------------------------------------------------------


def compute_arbitrary_dpd_pressure_contributions(particleType_,
                                                 coords_,
                                                 pMassDict_,
                                                 rc_,
                                                 forceKernels_,
                                                 cubeLen_,
                                                 moleculesChosen_=None):
    '''

    :param particleType_ : LAMMPS particle type
    :param coords_       : array of coordinates
    :param pMassDict_    : dict of the form {particleType_ : mass}
    :param rc_           : cutoff distance
    :param forceKernels_ : a list of symbolic functions the form f(r) where r is [interparticle distance]/rc_
                           (reduced distance)
    :param cubeLen_      :
    :param moleculesChosen_: index array for chosen . If not pased, all molecules are considered
    :return              :
    '''

    # for now, assume that the force kernel is only a function of a single variable (r)
    symbolList_ = list(forceKernels_[0].free_symbols)
    assert(len(symbolList_) == 1)
    rSym_ = symbolList_[0]

    for fk_ in forceKernels_:
        assert(symbolList_ == list(fk_.free_symbols))

    # assemble list of functions to compute excess pressure contributions from each force kernel
    pExFuncList_  = []
    for fk_ in forceKernels_:
        pExpr_ = rSym_*fk_
        pExFuncList_.append( ufuncify(rSym_, pExpr_, backend='numpy') )

    nMol_ = particleType_.shape[0]
    nF_ = len(forceKernels_)

    if moleculesChosen_ is None:
        moleculesChosen_ = np.arange(nMol_)

    nDPD_ = moleculesChosen_.size
    assert(nDPD_ <= nMol_)
    # nDPD_ = nMol_/molPerDPDParticle
    # if( int(nDPD_) != nDPD_):
    #     msg = "number of molecules (" + str(nMol_) + "/)" + "not divisible by molecules per DPD particle ("+str(nDPD_)+")"
    #     raise ValueError(msg)
    # nDPD_ = int(nDPD_)
    #
    # # randomly choose what molecules to consider based on the number of molecules represented by a DPD particle
    # moleculesChosen_ = np.random.choice(np.arange(nMol_), size=(nDPD_), replace=False)
    # moleculesChosen_.sort()

    particleMass_ = np.ndarray(particleType_.shape) * np.nan

    for key in pMassDict_.keys():
        i_chosen = (particleType_ == key)

        particleMass_[i_chosen] = pMassDict_[key]

    comReduced_ = molecular_center_of_mass(coords_ / rc_, particleMass_)
    comReduced_ = comReduced_[moleculesChosen_]

    cubeLenReduced_ = cubeLen_ / rc_
    volReduced_ = cubeLenReduced_ ** 3

    rcReduced_ = 1

    avgFarNeighbors_ = 0


    pressureContributions_ = []
    for iF_ in range(nF_):
        pressureContributions_.append(np.ndarray([nDPD_]) * np.nan)

    for iDPD in range(nDPD_):
        #         print('molecule: ', iMol+1, '/', nMol_)
        r_ij = compute_r_ij(comReduced_, iDPD, cubeLenReduced_)
        #     print("\tr_min:", rr.min())
        #     print("\tr_max:", rr.max())

        isNeighbor = r_ij < rcReduced_
        isNeighbor[iDPD] = False  # a particle is not a neighbor of itself

        tmp = np.sum(isNeighbor)
        avgFarNeighbors_ += tmp
        #         print('\tfar neighbors:  ', tmp)
        for iF_ in range(nF_):
            pExFunc_ = pExFuncList_[iF_]
            pressureContributions_[iF_][iDPD] = np.sum( pExFunc_(r_ij[isNeighbor] / rcReduced_) )

    avgFarNeighbors_ /= nDPD_
    print('avg. far   neighbors: ', avgFarNeighbors_)

    # pressure contributions multiplied by number of molecules, nMol_.
    # this is done to ensure
    # [mean excess pressure] = mean[ per-molecule pressure contributions ]
    # rather than
    # [mean excess pressure] =  sum[ per-molecule pressure contributions ]

    for iF_ in range(nF_):
        pressureContributions_[iF_] *= nDPD_ / (6 * volReduced_)

    return np.array(pressureContributions_)


# --------------------------------------------------------------------------------------------

def p_excess_dpd_from_components(pComponents_, *coeffs_):
    '''
    computes an excess pressure, P_ex, given some precomputed components of P_ex and
    coefficients (*coeffs_) to weight each component.

    :param pComponents_: precomputed components of excess pressure
    :param coeffs_: coefficients multiplying each pressure component
    :return: excess pressure
    '''
    if len(coeffs_) > 1:
        assert (len(coeffs_) == len(pComponents_))

    result_ = 0;
    for coeff_, p_ in zip(coeffs_, pComponents_):
        result_ += coeff_ * p_
    return result_


# --------------------------------------------------------------------------------------------

def compute_optimal_dpd_params( pExComponents_, pExcessMD_, bounds_=None):

    initGuess_ = [1 for i in range(len(pExComponents_))]
    numComp = len(pExComponents_)

    if bounds_ is None:
        upperBounds_ = np.array([ np.inf for i in range(numComp)])
        lowerBounds_ = np.array([-np.inf for i in range(numComp)])
        bnds_ = (lowerBounds_, upperBounds_)
    else:
        bnds_ = bounds_
        for i, lb in enumerate(bnds_[0]):
            ub = bnds_[1][i]
            if initGuess_[i] < lb : initGuess_[i] = lb
            if initGuess_[i] > ub : initGuess_[i] = ub

    indepVars_ = pExComponents_[0].ravel()

    for i in range(1, len(pExComponents_)):
        indepVars_ = np.vstack((indepVars_, pExComponents_[i].ravel()))


    optParams_, covar_ = curve_fit(p_excess_dpd_from_components,
                                   indepVars_,
                                   pExcessMD_.ravel(),
                                   p0=initGuess_,
                                   bounds=bnds_)

    return optParams_


# --------------------------------------------------------------------------------------------

def interp_lagrangian_to_eulerian(prop_, coords_, dims_, resolution_):
    '''
    :param prop_: a Lagrangian property with dimensions [prop]
    :param coords_: Lagrangian coordinates with dimensions [x, y, z]
    :param dims_: an iterable with values [xLength, yLength, zLength]
    :param resolution_: an iterable with values [xRes, yRes, zRes]
    :return: an eulerian interpolation of property, prop_
    '''
    s_ = prop_.shape

    assert (len(s_) + 1 == coords_.ndim)
    assert (coords_.shape[-1] == 3)

    # define a grid
    eulerianProp_ = np.ndarray(resolution_)

    # number of observations in that grid cell
    eulerianNum_ = np.ndarray(resolution_)

    x_ = np.linspace(0, dims_[0], resolution_[0] + 1)
    y_ = np.linspace(0, dims_[1], resolution_[1] + 1)
    z_ = np.linspace(0, dims_[2], resolution_[2] + 1)

    for i_x in range(resolution_[0]):
        inSliceX_ = (coords_[..., 0] >= x_[i_x]) & (coords_[..., 0] < x_[i_x + 1])
        coordsSliceX_ = coords_[inSliceX_]
        propSliceX_ = prop_[inSliceX_]

        for i_y in range(resolution_[1]):
            inSliceXY_ = (coordsSliceX_[..., 1] >= y_[i_y]) & (coordsSliceX_[..., 1] < y_[i_y + 1])
            coordsSliceXY_ = coordsSliceX_[inSliceXY_]
            propSliceXY_ = propSliceX_[inSliceXY_]

            for i_z in range(resolution_[2]):
                inSliceXYZ_ = (coordsSliceXY_[..., 2] >= z_[i_z]) & (coordsSliceXY_[..., 2] < z_[i_z + 1])
                eulerianProp_[i_x, i_y, i_z] = np.mean(propSliceXY_[inSliceXYZ_])
                eulerianNum_[i_x, i_y, i_z] = np.sum(inSliceXYZ_)

    return eulerianProp_, eulerianNum_


# --------------------------------------------------------------------------------------------

def interp_lagrangian_to_eulerian_over_time(prop_, coords_, dims_, resolution_):
    '''
    :param prop_: a Lagrangian property with dimensions [time, prop]
    :param coords_: Lagrangian coordinates with dimensions [time, x, y, z]
    :param dims_: an iterable with values [xLength, yLength, zLength]
    :param resolution_: an iterable with values [xRes, yRes, zRes]
    :return: an eulerian interpolation of property, prop_
    '''

    # define a grid
    nTimes_ = prop_.shape[0]
    eulerianProp_ = np.ndarray([nTimes_] + resolution_)

    # number of observations in that grid cell
    eulerianNum_ = np.ndarray([nTimes_] + resolution_)
    for i_time in range(nTimes_):
        print(i_time + 1, '/', nTimes_)
        eulerianProp_i, eulerianNum_i = interp_lagrangian_to_eulerian(prop_[i_time], coords_[i_time], dims_,
                                                                      resolution_)

        eulerianProp_[i_time] = eulerianProp_i
        eulerianNum_[i_time] = eulerianNum_i

    return eulerianProp_, eulerianNum_


# --------------------------------------------------------------------------------------------

def get_pressure_reduction_factor(rCut_, temperature_):
    '''
    computes conversion factor for taking pressure (in Pa) to its reduced value
    given a cutoff radius and temperature
    :param rCut -- cutoff radius (Angstroms)
    :param temperature_ -- temperature (Kelvin)
    '''
    angstromPerMeter_ = 1e10
    return(rCut_/angstromPerMeter_)**3/(constants.Boltzmann*temperature_)
